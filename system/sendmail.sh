#!/bin/sh
#mainscript which is excuted after each pull

#send email notification with outgoing and internal IP address
ipoutgoing=`curl icanhazip.com`
ipinternal=`/sbin/ifconfig | /bin/grep -A 1 'eth0' | /usr/bin/tail -1 | /usr/bin/cut -d ':' -f 2 | /usr/bin/cut -d ' ' -f 1`
{
    echo Subject: Quboo-Box: $ipoutgoing successfully updated
    echo Internal IP Adress: $ipinternal
} | /usr/sbin/ssmtp technik@quboo.ch

#sleep 1m
#echo "test" >~/logfile.log
